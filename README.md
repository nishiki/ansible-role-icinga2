# Ansible role: Icinga2

[![Version](https://img.shields.io/badge/latest_version-1.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-icinga2/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-icinga2/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-icinga2/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-icinga2/actions?workflow=molecule.yml)

Install and configure Icinga2

## Requirements

- Ansible >= 2.9
- Debian
  - Bookworm

## Role variables

- `icinga2_db_type` - set the database type mysql or postgresql (default: `mysql`)
- `icinga2_ido_host` - set the database host (default: `127.0.0.1`)
- `icinga2_ido_database` - set the database name (default: `icinga2`)
- `icinga2_ido_user` - set the database user (default: `icinga2`)
- `icinga2_ido_password` - set the database password
- `icinga2_master` - set if the server is a master node (default: `false`)
- `icinga2_endpoints` - hash with the endpoint name and address

```
  local.loc: 127.0.0.1
  satellite.loc: 10.121.30.20
```

- `icinga2_zones` - hash with zone configuration

```
  master:
    endpoints:
      - loca.loc
  satellite:
    parent: master
    endpoints:
      - satellite.loc
```

- `icinga2_templates` - hash with template configuration

```
  generic-host:
    type: host
    vars:
      os: Linux
    options:
      check_command: hostalive
      check_interval: 1m
      retry_interval: 30s
      max_check_attempts: 3
  generic-service:
    type: service
    options:
      max_check_attempts: 5
      check_interval: 1m
      retry_interval: 30s
```

- `icinga2_service_groups` - hash with service group configuration

```
  smnp:
    display_name: SMNP
    match: 'snmp-*'
```

- `icinga2_services` - hash with service configuration

```
  ping:
    check_command: ping
    assign: host.address
  ssh:
    check_command: ssh
    assign: (host.address || host.address6) && host.vars.os == "Linux"
    vars:
      timeout: 5
```

- `icinga2_host_groups` - hash with host group configuration

```
  tomcat:
    assign: host.vars.tomcat_port
  influxdb:
    display_name: InfluxDB
    assign: match("influx-*", host.name)
```

- `icinga2_hosts` - hash with host configuration

```
  localhost:
    template: generic-host
    address: 127.0.0.1
    vars:
      dist: Debian
```

- `icinga2_user_groups` - hash with user group configuration

```
  admin:
    display_name: Administrateur
```

- `icinga2_users` - hash with the user configuration

```
  johndoe:
    display_name: John Doe
    email: john@doe.loc
    groups:
      - admin
```

- `icinga2_api_users` - hash with the user configuration for the API access

```
  icingaweb2:
    password: secret
    permissions:
      - '*'
```

- `icinga2_timeperiods` - hash with timeperiods configuration

```
  24x7:
    ranges:
      monday: 00:00-24:00
      tuesday: 00:00-24:00
      wednesday: 00:00-24:00
      thursday: 00:00-24:00
      friday: 00:00-24:00
      saturday: 00:00-24:00
      sunday: 00:00-24:00
    excludes:
      monday: 00:05-00:10
  default_exclude:
    prefer_includes: false
    ranges:
      monday: 00:00-01:00
    includes:
      monday: 00:05-00:10
```

- `icinga2_dependencies` - hash with dependencies configuration ([view all options](https://icinga.com/docs/icinga-2/latest/doc/09-object-types/#dependency))

```
  postgresql-port:
    parent_host_name: host.name
    parent_service_name: postgresql-service
    child_service_name: postgresql-port
    disable_notifications: true
    states:
      - DOWN
    assign: '"postgresql" in host.groups'
```

- `icinga2_notifications` - hash with notifications configuration

```
  mail-icingaadmin-host:
    type: Host
    import: mail-host-notification
    assign: host.vars.notification.mail
    user_groups: host.vars.notification.mail.groups
    users: host.vars.notification.mail.users
  mail-icingaadmin-service:
    type: Service
    import: mail-service-notification
    assign: host.vars.notification.mail
    user_groups: host.vars.notification.mail.groups
    users: host.vars.notification.mail.users
    options:
      period: $service.vars.period$
```

- `icinga2_notification_commands` - hash with notification command configuration

```
  mail-service-notification:
    script: mail-service-notification.sh
    env:
      NOTIFICATIONTYPE: $notification.type$
      SERVICENAME: $service.name$
      HOSTNAME: $host.name$
      HOSTDISPLAYNAME: $host.display_name$
      HOSTADDRESS: $address$
      SERVICESTATE: $service.state$
      LONGDATETIME: $icinga.long_date_time$
      SERVICEOUTPUT: $service.output$
      NOTIFICATIONAUTHORNAME: $notification.author$
      NOTIFICATIONCOMMENT: $notification.comment$
      SERVICEDISPLAYNAME: $service.display_name$
      USEREMAIL: $user.email$
      MAILFROM: $notification_from$
```

- `icinga2_check_commands` - hash with check command configuration

```
  ping-ha:
    command: '"/bin/ping"'
    arguments:
      packet:
        value: 10
        required: true
    vars:
      timeout: 10
```

- `icinga2_event_commands` - hash with event command configuration (see check command config)
- `icinga2_features` - hash with the features to enable and thier configuration

```
  api:
    object: ApiListener
    config:
      accept_config: false
      accept_commands: true
```

- `icinga2_scripts` - hash with the scripts to use by icinga

```
  log-file: |
    #!/bin/bash
    echo "$(date) $HOSTDISPLAYNAME" >> /tmp/notification.log
```

## How to use

```
- hosts: server
  roles:
    - icinga2
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
