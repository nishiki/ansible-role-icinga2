# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added

- add dependencies
- add options in host

### Changed

- test: use personal docker registry

### Fixed

- bug in check command template
- bug in user template
- add multiple value type in service vars

### Removed

- test: remove support debian11

## v1.0.0 - 2021-08-15

- first version
