import testinfra.utils.ansible_runner

def test_packages(host):
  for package_name in ['icinga2', 'icinga2-ido-mysql', 'monitoring-plugins']:
    package = host.package(package_name)
    assert package.is_installed

def test_config_hosts(host):
  config = host.file('/etc/icinga2/conf.d/hosts.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('address = "127.0.0.1"')

def test_config_services(host):
  config = host.file('/etc/icinga2/conf.d/services.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('check_command = "ping"')

def test_config_templates(host):
  config = host.file('/etc/icinga2/conf.d/templates.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('os = "Linux"')

def test_config_commands(host):
  config = host.file('/etc/icinga2/conf.d/commands.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('NotificationCommand "date"')

def test_config_check_commands(host):
  config = host.file('/etc/icinga2/conf.d/check_commands.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('value = "$http_vhost$"')

def test_config_database(host):
  config = host.file('/etc/icinga2/conf.d/ido-db.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('password = "test"')

def test_config_timeperiods(host):
  config = host.file('/etc/icinga2/conf.d/timeperiods.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('"sunday" = "00:00-24:00"')

def test_config_notifications(host):
  config = host.file('/etc/icinga2/conf.d/notifications.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('user_groups = host.vars.notification.mail.groups')

def test_config_dependencies(host):
  config = host.file('/etc/icinga2/conf.d/dependencies.conf')
  assert config.exists
  assert config.is_file
  assert config.user == 'root'
  assert config.group == 'nagios'
  assert config.mode == 0o640
  assert config.contains('parent_host_name = host.name')

def test_script(host):
  path = host.file('/etc/icinga2/scripts/test-notification.sh')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'nagios'
  assert path.mode == 0o750
  assert path.contains('date >> /tmp/notification.log')

def test_service(host):
  service = host.service('icinga2')
  assert service.is_running
  assert service.is_enabled
